import React from 'react'
import {Navbar, NavbarBrand} from 'reactstrap'
import Login from './LoginA';
import Register from './RegisterR';
function MainPage() {
  return (
    <div>
        <>
         <Navbar
    className="my-2"
    color="dark"
    dark
  >
    <NavbarBrand href="/">
      <img
        alt="logo"
        src="/logo-white.svg"
        style={{
          height: 40,
          width: 40
        }}
      />
    </NavbarBrand>
    <Login />
    <Register />
  </Navbar>
  </>
        </div>
  )
}

export default MainPage