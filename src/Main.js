import React from 'react'
import {Card,CardImgOverlay,CardImg,CardTitle,Button} from 'reactstrap'

function Main() {
   
  return (

 <div>
      <center>
    <h1 color='red'>FIND BLOOD DONORS</h1>
        <br></br>
       
   <table>
    <tbody>
      <tr>
        <td width={"120"} height={"30"} className='"contactheading'> BloodGroup</td>
        <td width={300}>
            <select required>
       {/* <option value={"select"}>select</option> */}
        <option value={"A+"}>A+</option>
        <option value={"A-"}>A-</option>
        <option value={"A1+"}>A1+</option>
        <option value={"A1-"}>A1-</option>
        <option value={"A1B+"}>A1B+</option>
        <option value={"A1B-"}>A1B-</option>
        <option value={"A2+"}>A2+</option>
        <option value={"A2-"}>A2-</option>
        <option value={"A2B+"}>A2B+</option>
        <option value={"A2B-"}>A2B-</option>
        <option value={"AB+"}>AB+</option>
        <option value={"B+"}>B+</option>
        <option value={"B-"}>B-</option>
        <option value={"Bombay Blood Group"}>Bombay Blood Group</option>
        <option value={"INRA"}>INRA</option>
        <option value={"O+"}>O+</option>
        <option value={"O-"}>O-</option>
    
    </select>
</td>
</tr>
 <tr>
<td height={"30"} className='contactheading' width={"120"}>Country</td>
<td width={"300"}>
    <select required>
      <option value={"select"}>select</option> 
        <option value={"1"}>India</option>
        <option value={"2"}>Australia</option>
        <option value={"3"}>America</option>
    </select>
</td>
</tr> 

<tr>
<td height={"30"} className='contactheading' width={"120"}>State</td>
<td width={"300"}>
   <select required>
        <option value={"select"}>select</option> 
        <option value={"1"}>Telangana</option>
        <option value={"2"}>Andhra Pradesh</option>
        <option value={"3"}>Kerala</option>
    </select>
</td>
</tr>
 <tr>
<td height={"30"} className='contactheading' width={"120"}>District</td>
<td width={"300"}>
   <select required>
       <option value={"select"}>select</option> 
        <option value={"1"}>Karimnagar</option>
        <option value={"2"}>Hanamkonda</option>
        <option value={"3"}>Siddipet</option>
    </select>
</td>
</tr> 
{/* <tr>
<td height={"30"} className='contactheading' width={"120"}>City</td>
<td width={"300"}>
   <select required>
       <option value={"select"}>select</option> 
        <option value={"1"}>Husnabad</option>
        <option value={"2"}>Kothapally</option>
        <option value={"3"}>Huzurabad</option>
    </select>
</td>
</tr> */}
<br></br>
<tr>
<td height={"30"}>&nbsp;</td>
<td align='middle'>
<table width={"150"} border={"0"} cellSpacing={"0"} cellPadding={"0"}>
<tbody>
    <tr>
<td width={"30"}>&nbsp;</td>
<td>
   
<Button color="danger" style={{MarginLeft:'30px'}} href="Search">Search</Button> 
</td>
 </tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center>
   <br></br>
   <br></br>
   <div>
    <img src='https://img.freepik.com/premium-photo/old-black-background-grunge-texture-dark-wallpaper-blackboard-chalkboard-room-wall_959084-10.jpg' alt='Donate Blood' width={"100%"} height={"1px"}></img>
</div>

   <div class="rich-text-editor-content" style={{paddingTop:"10px"}}>
    <h2>What is done during the test and what is measured</h2>
     <p>Prior to donating blood, all donors will receive a free health screening. At the time of your donation, your blood pressure, hemoglobin and pulse will be checked. We record these vitals in your online donor profile. You can access this information as well as past health information obtained during prior donations, at any time. We encourage you to share your results with your healthcare provider at your next visit.</p></div>
  
   <div class="container-flex icon">
           <center><img loading="lazy" class="icon-image" src="https://www.redcrossblood.org/content/dam/redcrossblood/african-american-blood-donors/Blood-Pressure.png.transform/282/q70/feature/image.png"/>
       </center> </div>
        <h4 class="card-title" style={{color:"rgba(51,51,51,1)"}}><u>Blood Pressure</u></h4>
        <br></br>
        <div class="card-description text">
        <p>The Red Cross checks your blood pressure before every donation. When untreated, high blood pressure can increase the risk of heart attack, stroke and other health complications. Also known as <a href="https://www.redcrossblood.org/donate-blood/dlp/blood-pressure-donors.html">hypertension</a>, the condition usually has no symptoms, and is often called the "silent killer."</p></div>
     <div class="card-button icon-link-button"></div>

     <div class="container-flex icon">
           <center><img loading="lazy" class="icon-image" src="https://www.redcrossblood.org/content/dam/redcrossblood/african-american-blood-donors/Pulse.png.transform/282/q70/feature/image.png"/></center> </div>
        <h4 class="card-title" style={{color:"rgba(51,51,51,1)"}}><u>Pulse</u></h4>
        <br></br>
        <div class="card-description text">
        <p>Your pulse will be checked before every Red Cross blood donation. Staff will measure the number of times your heart beats per minute and count any irregular heartbeats. Your pulse rate gives insights into your heart rhythm and how well your heart may be working. A heart rate that is too fast, too slow, or irregular can indicate an underlying condition.</p></div>
     <div class="card-button icon-link-button"></div>

     <div class="container-flex icon">
         <center>   <img loading="lazy" class="icon-image" src="https://www.redcrossblood.org/content/dam/redcrossblood/african-american-blood-donors/Hemoglobin-Levels.png.transform/282/q70/feature/image.png"/></center></div>
            <h4 class="card-title" style={{color:"rgba(51,51,51,1)"}}><u>Hemoglobin</u></h4>
            <br></br>
            <div class="card-description text">
        <p>The Red Cross checks your hemoglobin before every donation to ensure that you are healthy enough to donate. Hemoglobin is a protein that contains <a href="https://www.redcrossblood.org/donate-blood/blood-donation-process/before-during-after/iron-blood-donation.html">iron</a> and carries oxygen to the tissues in your body. Iron is essential to help your body to replace new red blood cells lost through blood donations.</p></div>
        <div class="card-button icon-link-button"></div>


<footer class="container-fluid text-center">
            <div class="col-md-12" style={{backgroundColor:'#333',padding:'25px',color:'#000000'}} >
    <div class="col-md-6" style={{paddingTop:'20px'}}>
        <a href="tips.php">Tips</a> | <a href="refer.php">Refer a Friend</a> |  <a href="privacy_policy.php"> Privacy Policy</a>
    </div>
</div>
<div class="col-md-12 center" style={{fontSize: '0.7rem', backgroundColor:'#000000', padding: '15px',color: '#000000'}}>© 2020 Copyright <a href="https://vindia.net" target="_blank">V India Technologies Private Limited</a></div>
</footer>
  
    

    </div>
  )
}

export default Main