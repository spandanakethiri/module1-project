import './App.css';
//import ClassComponent from './ClassComponent';
//import FunctionalComponent from './FunctionalComponent';
//import MainPage from './MainPage';
//import Assignment from './Assignment';
//import Ftable from './Table';
//import Login from './Login';


//import AboutUs from './AboutUs';
import Home from './Components/Home';
import { BrowserRouter, Routes,Route,Router,Link } from 'react-router-dom';
import AboutUs from './Components/AboutUs';
import Facts from './Components/Facts';
 import Search from './Components/Search';
import Main from './Main';
import Home1 from './Components/Home1';
import Message from './Components/Message';

function App() {
  return (
    <div>
  
         {/* <ClassComponent/>*/}
        {/*<FunctionalComponent/>*/}
       {/*<MainPage />*/}
        {/* <Login /> */}
        {/*<Assignment />*/}
       
       {/*<Ftable />*/}
      {/* <Project />  */}
      {/* <Search /> */}
        {/*<AboutUs />*/}
        {/* <Home /> */}
        <Home1 /><br></br><br></br>
     <br></br>
<BrowserRouter>
 <Routes>
   <Route path="/" element={<Home/>}/> 
   <Route path="/Home" element={<Home/>}/>
   <Route path="Main" element={<Main/>}/>
   <Route path="/AboutUs" element={<AboutUs/>}/>
   <Route path="/Facts" element={<Facts/>}/>
   <Route path="/Search" element={<Search/>}/>
   <Route path="/Message" element={<Message/>}/>
  
  </Routes>
  </BrowserRouter> 
    </div>
  );
}

export default App;
