import React from 'react'
import {Navbar,NavbarBrand} from 'reactstrap'
function Facts() {
  return (
    <div>
  <Navbar
    className="my-2"
    color="secondary"
    dark>
    <NavbarBrand href="/">
   <h4><b>BLOOD DONATION FACTS</b></h4>
   </NavbarBrand>
   </Navbar>
<p>
<h6><u>FACTS ABOUT BLOOD NEEDS-</u></h6>
</p>
<ul>
   <li>Every year our nation requires about 5 Crore units of blood, out of which only a meager 2.5 Crore units of blood are available.</li>
<div style={{marginRight:"500px"}}><center>
<img src='https://www.friends2support.org/imgs/blood_3.jpg'></img></center>
</div>
   <li>The gift of blood is the gift of life. There is no substitute for human blood.</li>
   <li>The blood type most often requested by hospitals is Type O.</li>
   <li>More than 1 million new people are diagnosed with cancer each year. Many of them will need blood, sometimes daily, during their chemotherapy treatment.</li>
   <li>A single car accident victim can require as many as 100 units of blood</li>
</ul>
<p>
<h6><u>FACTS ABOUT THE BLOOD SUPPLY-</u></h6>  
</p>
<ul>
<li>Blood cannot be manufactured–it can only come from generous donors.</li>
<div style={{marginRight:"500px"}}><center>
<img src='https://www.friends2support.org/imgs/blood_4.jpg'></img></center>
</div>
<li>Type O-negative blood (red cells) can be transfused to patients of all blood types. It is always in great demand and often in short supply.</li>
<li>Type AB-positive plasma can be transfused to patients of all other blood types. AB plasma is also usually in short supply.</li>
</ul>
<p>
<h6><u>FACTS ABOUT DONORS-</u></h6>  
</p>
<ul>
<li>The number one reason donors say they give blood is because they "want to help others."</li>
<div style={{marginRight:"500px"}}><center>
<img src='https://www.friends2support.org/imgs/blood_5.jpg'></img></center>
</div>
<li>The number one reason donors say they give blood is because they "want to help others."</li>
<li>Two most common reasons cited by people who don't give blood are: "Never thought about it" and "I don't like needles."</li>
<li>Type O-negative blood is needed in emergencies before the patient's blood type is known and with newborns who need blood.</li>
<li>Only 7 percent of people in India have O-negative blood type. O-negative blood type donors are universal donors as their blood can be given to people of all blood types.</li>
<li>Thirty-five percent of people have Type O (positive or negative) blood.</li>
<li>0.4 percent of people have AB-blood type. AB-type blood donors are universal donors of plasma, which is often used in emergencies, for newborns and for patients requiring massive transfusions.</li>
</ul>
<p>There are four main blood types: A, B, AB and O.</p>
<footer class="container-fluid text-center">
            <div class="col-md-12" style={{backgroundColor:'#333',padding:'25px',color:'#000000'}} >
    <div class="col-md-6" style={{paddingTop:'20px'}}>
        <a href="tips.php">Tips</a> | <a href="refer.php">Refer a Friend</a> |  <a href="privacy_policy.php"> Privacy Policy</a>
    </div>
</div>
<div class="col-md-12 center" style={{fontSize: '0.7rem', backgroundColor:'#000000', padding: '15px',color: '#000000'}}>© 2020 Copyright <a href="https://vindia.net" target="_blank">V India Technologies Private Limited</a></div>
</footer>
    </div>
  )
}

export default Facts