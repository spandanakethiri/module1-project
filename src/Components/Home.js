import React from 'react'
import {Card,Button,CardBody,CardTitle,CardSubtitle,CardText} from 'react-bootstrap'
function Home() {
  return ( 
<div className='row' style={{ backgroundColor: '#FFFFFF',paddingTop:'50px',height:'200px'}}>
<div className='col-md-4' style={{ textAlign: 'center',color: '#f6f6f6',paddingTop:'2px'}}>
<h3 style={{ color: 'black'}}>
<div>A Drop of water makes ocean.</div>
<div>A Unit of Blood SAVES LIFE. </div>
</h3>

</div>
<br></br>
<div>
    <img src='http://blooddonors.in/img/donate1.jpg' alt='Donate Blood' width={"100%"}></img>
</div>
<br></br>
<br></br>
<Card className="text-center">
      <Card.Body>
        <Card.Title></Card.Title>
      </Card.Body>
  </Card>
     <div style={{marginLeft:"500px"}}><h1><b>Blood Products</b></h1></div> 
     <div style={{marginLeft:"150px"}}><h4>We provide hospitals with the following lifesaving blood products 24 hours a day, 365 days a year.</h4></div> 
    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.redcrossblood.org/content/dam/redcrossblood/rcb/biomedical-services/components/red-blood-cells.jpg.transform/1288/q82/feature/image.jpeg"

  />
  <CardBody>
    <CardTitle tag="h5">
    Red Blood Cells
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   <ul>
    <li>Leukocyte-reduced</li>
    <li>CMV negative</li>
    <li>Irradiated</li>
    <li>Deglycerolized</li>
    <li>Washed</li>
    <li>For pediatric use</li>
    <li>Frozen</li>
    <li>Antigen negative </li>
   </ul>
    </CardText>
  </CardBody>
</Card>
</div> <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.redcrossblood.org/content/dam/redcrossblood/rcb/biomedical-services/components/platelets.jpg.transform/1288/q82/feature/image.jpeg"

  />
  <CardBody>
    <CardTitle tag="h5">
   Platelets
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   <ul>
    <li>Platelets pheresis, leukocyte-reduced</li>
    <li>Platelets pheresis irradiated</li>
    <li>Platelets pheresis, CMV-negative</li>
    <li>Platelets pheresis,HLA matched</li>
    <li>Platelets pheresis,HPA-1a(PLA)negative</li>
    {/* <li>Platelet additive solution(PAS)platelets</li> */}
    {/* <li>Pathogen reduced platelets(in PAS)</li> */}
   </ul>
    </CardText>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.redcrossblood.org/content/dam/redcrossblood/rcb/biomedical-services/components/plasma-blood.jpg.transform/1288/q82/feature/image.jpeg"

  />
  <CardBody>
    <CardTitle tag="h5">
   Plasma
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   <ul>
    <li>Plasma frozen within 24 hours(IPF24)</li>
    <li>Plasma held at room temperature for upto 24 hours(PF24RT24)</li>
    <li>Fresh frozen plasma(FFP)</li>
    <li>Plasma, cryoprecipitate reduced</li>
   </ul>
    </CardText>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.redcrossblood.org/content/dam/redcrossblood/rcb/biomedical-services/components/whole-blood.jpg.transform/1288/q82/feature/image.jpeg"

  />
  <CardBody>
    <CardTitle tag="h5">
    Whole Blood
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
   <br></br>
    <CardText>
   <ul>
    <li>Leukocyte-reduced and non-leukocyte reduced</li>
    <br></br>
    <br></br>
    <li>Other available features: CMV negative, Irradiated, for pediatric use</li>
   </ul>
    </CardText>
  </CardBody>
</Card>
</div>

    {/* <div class="section-header-description">
    <h2 class="section-header-title center" data-aa-temporary-homepage-section-title="temp home page section title not set"><b><center>Blood Products</center></b></h2>
    <div class="section-header-subtitle center" style={{marginLeft:"300px"}}>We provide hospitals with the following lifesaving blood products 24 hours a day, 365 days a year. </div>
  </div>
  <br></br>
  <br></br>
<div class="object-fit-image-container" style={{marginLeft:"500px"}}>
 <img loading="lazy" src="https://www.redcrossblood.org/content/dam/redcrossblood/rcb/biomedical-services/components/red-blood-cells.jpg.transform/1288/q82/feature/image.jpeg" class="featured-image"/>
</div>
<div class="card-info">
   <div class="title">Red Blood Cells</div>
 <div class="description text"><ul>
<li>Leukocyte-reduced<br/>
</li>
<li>CMV negative</li>
<li>Irradiated</li>
<li>Deglycerolized</li>
<li>Washed</li>
<li>For pediatric use</li>
<li>Frozen</li>
<li>Antigen negative&nbsp;</li>
</ul>
</div>
</div> */}


<footer class="container-fluid text-center">
            <div class="col-md-12" style={{backgroundColor:'#333',padding:'25px',color:'#000000'}} >
    <div class="col-md-6" style={{paddingTop:'20px'}}>
        <a href="tips.php">Tips</a> | <a href="refer.php">Refer a Friend</a> |  <a href="privacy_policy.php"> Privacy Policy</a>
    </div>
</div>
<div class="col-md-12 center" style={{fontSize: '0.7rem', backgroundColor:'#000000', padding: '15px',color: '#000000'}}>© 2020 Copyright <a href="https://vindia.net" target="_blank">V India Technologies Private Limited</a></div>
</footer>
    </div>
  )
}
export default Home