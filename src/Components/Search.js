import React, { useEffect, useState }  from 'react'
import {Table,Button} from 'reactstrap';
import axios from 'axios'
function Search() {
    const [data, setData] = useState([]);
    useEffect(() => {
      const fetchData = async () => {
        try {
          let response = await axios.get("http://localhost:8000/fetch");
          console.log(response.data);
          setData(response.data);
        } catch (err) {
          throw err
        }
      };
      fetchData();
    }, []); 
    const handleSendMail = (email) => {
      alert(`Sent mail to ${email}`);
    };
    
  return (
    <div>
        <Table>
      <thead>
         <tr>
         <th>BloodGroup</th>
          <th>Country</th>
          <th>State</th>
          <th>District</th>
          <th>City</th>
          <th>Email</th>
         </tr>
      </thead>
        <tbody>
           {data.map((item)=>(
          <tr key={item.Email}>
            <td>{item.BloodGroup}</td>
            <td>{item.Country}</td>
            <td>{item.State}</td>
            <td>{item.District}</td>
            <td>{item.City}</td>
            <td>{item.Email}</td>
            <td><Button
                  className='btn btn-primary'
                  onClick={() => handleSendMail(item.Email)}>Send Mail</Button></td>
           </tr>
           ))} 
        </tbody>
           </Table> 
    </div>
  )
}
export default Search