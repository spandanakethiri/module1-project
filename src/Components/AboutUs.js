import React from 'react'
import {Card,Button,Navbar,NavbarBrand} from 'reactstrap';
import {useState} from 'react';


// import Card from 'react-bootstrap/Card';

function AboutUs() {
    const [selectedOption, setSelectedOption] = useState('');

  // Event handler for changing the selected option
  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
    
  };
  return (
  <div>
     <Navbar
    className="my-2"
    color="secondary"
    dark>
    <NavbarBrand href="/">
   <h1>ABOUT US</h1>
   </NavbarBrand>
   </Navbar>
   
    <p>
    Have you at anytime witnessed a relative of yours or a close friend searching frantically for a blood donor, when blood banks say out of stock, the donors in mind are out of reach and the time keeps ticking? Have you witnessed loss of life for the only reason that a donor was not available at the most needed hour? Is it something that we as a society can do nothing to prevent? This thought laid our foundation.
    </p>
    
   <h4><b><u>VISION:</u></b></h4>
   
   <p>
   To pave way for a safer and better tommorrow.
   Safer, by bringing blood donors and those in need to a common platform.
   Better, by providing every person what he/she deserves the most, best education.
   Our aim in next 5 years,
   To be the real hope of every Indian in search of a voluntary blood donor.
   To set up a well organised infrastructure through out the country to cater to the education of the under resourced by way of maintaining a repository of contributed books and providing as many resources as possible for rural child education.
   </p>
   
   
   <h4><b><u>MISSION:</u></b></h4>
   
  <p>
   To make the best use of contemporary technologies in delivering a promising web portal to bring together all the blood donors in India; thereby fulfilling every blood request in the country.
   To provide a common platform for those who have a zeal to support education of the under resourced yet meritorious students
  </p>
  <Navbar
    className="my-2"
    color="secondary"
    dark>
    <NavbarBrand href="/">
   <h6>WHO CAN/CAN'T DONATE BLOOD</h6>
    </NavbarBrand>
  </Navbar>
<p><b>
  CAN...
  </b>
</p>
<p>
Let others benefit from your good health. Do donate blood if ...
</p>
<ul>
  <li>you are between age group of 18-60 years.</li>
  <li>your weight is 45 kgs or more.</li>
  <li>your haemoglobin is 12.5 gm% minimum.</li>
  <li>your last blood donation was 3 months earlier.</li>
  <li>you are healthy and have not suffered from malaria, typhoid or other transmissible disease in the recent past.</li>
</ul>
<p>We ensure the health of blood, before we take it, as well as after it is collected. Firstly, the donor is expected to be honest about his or her health history and current condition. Secondly, collected blood is tested for venereal diseases, hepatitis B & C and AIDS.</p>
<p>You have to be healthy to give 'safe blood'</p>

<p><b>
  CAN'T...
  </b>
</p>
<p>Do not donate blood if you have any of these conditions</p>
<ul>
  <li>cold / fever in the past 1 week.</li>
  <li>under treatment with antibiotics or any other medication.</li>
  <li>major surgery in the last 6 months</li>
  <li>shared a needle to inject drugs/ have history of drug addiction.</li>
  <li>been tested positive for antibodies to HIV.</li>
</ul>
<footer class="container-fluid text-center">
            <div class="col-md-12" style={{backgroundColor:'#333',padding:'25px',color:'#000000'}} >
    <div class="col-md-6" style={{paddingTop:'20px'}}>
        <a href="tips.php">Tips</a> | <a href="refer.php">Refer a Friend</a> |  <a href="privacy_policy.php"> Privacy Policy</a>
    </div>
</div>
<div class="col-md-12 center" style={{fontSize: '0.7rem', backgroundColor:'#000000', padding: '15px',color: '#000000'}}>© 2020 Copyright <a href="https://vindia.net" target="_blank">V India Technologies Private Limited</a></div>
</footer>
  </div>
     
   
  )
}

export default AboutUs