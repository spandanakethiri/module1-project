import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';
import Login from './Login'
function Register(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({
    Username:'',
    BloodGroup:'',
    Email:'',
    Password:'',
    Availability:'',
    Country:'',
    City:'',
    District:'',
    State:'',
    Pincode:''

});
const handleInput = (e) =>{
    const {name, value} = e.target
    setFormdata({
        ...formdata,
        [name]:value
    })
}
console.log(formdata)
  const toggle = () => setModal(!modal);
  /*const toggle = () =>{
    e.preventDefault();
    setModal(!modal);
  }*/
  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      console.log(formdata);
      let response = await axios.post(`http://localhost:8000/register`,formdata)
      console.log(response)
      alert("data inserted");
    }catch (err){
      throw err
    }
   }
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        REGISTER
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Registration Form</ModalHeader>
        <ModalBody>

<Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleUsername">
        Username
        </Label>
        <Input
          id="exampleUsername"
          name="Username"
          placeholder="username"
          type="username"
          value={formdata.Username}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="example">
       BloodGroup
        </Label>
        <Input
          id="exampleBloodGroup"
          name="BloodGroup"
          placeholder="Bloodgroup"
          type="Bloodgroup"
          value={formdata.BloodGroup}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="Email"
          placeholder="with a placeholder"
          type="email"
          value={formdata.Email}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="Password"
          placeholder="password placeholder"
          type="password"
          value={formdata.Password}
          onChange={handleInput}
          required
          pattern="(?=.\d)(?+.[a-z])(?=.*[A-Z]).{8,}"
        />
      </FormGroup>
    </Col>
    </Row>
    <Col md={6}>
      <FormGroup>
        <Label for="example">
         Availability 	
        </Label>
        <Input
          id="exampleAvailability"
          name="Availability"
          placeholder=" "
          type="text"
          value={formdata.Availability}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    
   
    <Row>
    <FormGroup>
    <Label for="exampleCountry">
      Country
    </Label>
    <Input
      id="exampleCountry"
      name="Country"
      placeholder="India"
      value={formdata.Country}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleCity">
      City
    </Label>
    <Input
      id="exampleCity"
      name="City"
      placeholder="ex:knr"
      value={formdata.City}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleDistrict">
      District
    </Label>
    <Input
      id="exampleDistrict"
      name="District"
      placeholder=""
      value={formdata.District}
      onChange={handleInput}
      required
    />
  </FormGroup>
  </Row>
  <Row>
  <Col md={6}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="State"
          value={formdata.State}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePincode">
         Pincode
        </Label>
        <Input
          id="examplePincode"
          name="Pincode"
          value={formdata.Pincode}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup>
    <Button type='Sign in'>
      Sign in
    </Button>
  </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
          <Login />
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Register;