import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup } from 'reactstrap';
// import Register from './Components/Register'
function Login(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({//to store the data or object
    email:'',
    password:''
});
const handleInput = (e) =>{
    const {name, value} = e.target//which element we are targeting
    setFormdata({
        ...formdata,//... spread operator used to unpack the data
        [name]:value //[]used to update the data  
    })
}
console.log(formdata)
  const toggle = () => setModal(!modal);
  /*const toggle = () =>{
    e.preventDefault();
    setModal(!modal);
  }*/
  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      let response = await axios.get(`http://localhost:8000/login/${formdata.email}/${formdata.password}`)
      console.log(response)
      alert("login successfull");
    }catch (err){
      throw err
    }
   }
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        LOGIN
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>login page</ModalHeader>
        <ModalBody>
         
  <Form onSubmit={handleSubmit}>
    <FormGroup floating>
      <Input
        id="exampleEmail"
        name="email"
        placeholder="Email"
        type="email"
        value={formdata.email}
        onChange={handleInput}
        required
      />
      <Label for="exampleEmail">
        Email
      </Label>
    </FormGroup>
    {' '}
    <FormGroup floating>
      <Input
        id="examplePassword"
        name="password"
        placeholder="Password"
        type="password"
        value={formdata.password}
        onChange={handleInput}
        required
        // pattern="(?=.\d)(?+.[a-z])(?=.*[A-Z]).{8,}"
      />
      <Label for="examplePassword">
        Password
      </Label>
    </FormGroup>
    {' '}
    <Button type='Submit'>
      Submit
    </Button>
  </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
      {/* <Register /> */}
         
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Login;
////AXIOS pre defined library