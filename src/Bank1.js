const {createStore} = require ('redux');
//action types
const ADD_TODO = "ADD_TODO";
const REMOVE_TODO = "REMOVE_TODO";

const initialstate ={     //initialize
  todos:[],
};
const todoReducer = (state = initialstate, action) =>{     //reducer function
    const newstate = {...state};
    switch(action. type) {
        case "ADD_TODO":
            return {
                ...state,
                todos: [
                  ...state.todos,
                  {
                    id: action.id,
                    text: action.text,
                  },
                ],
              };
           
            case "REMOVE_TODO":
                return {
                    ...state,
                    todos: state.todos.filter((todo) => todo.id !== action.id),
                  };
                default:
                  return state;
              }
            };
              
 
const store =createStore(todoReducer)

store.subscribe(()=>{
    console.log("Current State:", store.getState())
})
const addTodo = (id, text) => ({
    type: ADD_TODO,
    id,
    text,
  });
  
  const removeTodo = (id) => ({
    type: REMOVE_TODO,
    id,
  });
  
  // Dispatch ADD_TODO action
  store.dispatch(addTodo(1, "abc"));
  store.dispatch(addTodo(2, "xyz"));
  
  // Dispatch REMOVE_TODO action
  store.dispatch(removeTodo(1));
