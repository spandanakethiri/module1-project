import React, { Component } from 'react'

export class ClassComponent extends Component {
constructor(props) {
  super(props)

  this.state = {
     count:0
  }
}
increment = () => {
this.setState({count:this.state.count+1});
}
decrement = () => {
  this.setState({count:this.state.count-1});
  if(this.state.count<1)
  this.setState({count:this.state.count=0});
}
reset = () => {
  this.setState({count:this.state.count=0});
}

//life cycle methods
componentDidMount(){
  console.log("mounting phase")
}
componentDidUpdate(){
  console.log("updating phase")
}


  render() {
    return (
      <div>
        <h1>count</h1>
        <h1>{this.state.count}</h1>
        <button onClick={this.increment}>Increment</button>
        <button onClick={this.decrement}>Decrement</button>
        <button onClick={this.reset}>Reset</button>
      </div>
    )
  }
}


export default ClassComponent