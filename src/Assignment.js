import React from 'react'
import {Navbar, NavbarBrand,CardTitle,CardText,CardImgOverlay,Card,CardImg,CardBody,Button,CardSubtitle} from 'reactstrap'
import Login from './Login';
import Register from './Register';
function Assignment() {
  return (
    <div>
        <>
         <Navbar
    className="my-2"
    color="dark"
    dark
  >
    <NavbarBrand href="/">
   <h3>SWIGGY</h3>
   <form class="d-flex" style={{width:500,marginTop:20}}>
              <input class="form-control me-sm-2" type="search" placeholder="Search"></input>
              <button class="btn btn-dark my-2 my-sm-0" type="submit">Search</button>
            </form>
    </NavbarBrand>
       <Login />
  <Register/>
  </Navbar>
  </>
  <div>
  <Card inverse>
    <CardImg
      alt="Card image cap"
      src="https://static.thearcweb.com/images/PROD/PROD-94d963ea-d966-47bf-a6a8-60518888a33e.jpeg"
      style={{
        backgroundColor:'light pink',
        height: 300
      }} 
      width="100%"
    />
    <CardImgOverlay>
      <CardTitle tag="h5">
      WELCOME TO SWIGGY..
      </CardTitle>
      <CardText>
        Order Food From Favourite Restaurents Near You.
      </CardText><br></br>
      <br></br>
      <br></br>
      <br></br>
      <CardText>
        <small className="text-muted">
         <h3>POPULAR CITIES IN INDIA</h3>
         Ahmedabad,Hyderabad,Chennai,Banglore,Delhi,etc..
        </small>
         
       </CardText>
    </CardImgOverlay><br></br>
  </Card></div>

 <br></br>
  <div class="row ">
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem',
    height: '40'
  }}
>
  <img
    alt="Sample"
    src="https://files.yappe.in/place/full/pista-house-bidar-3494247.webp"/>
  <CardBody>
    <CardTitle tag="h5">
   PISTA HOUSE RESTAURENT
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6">
   </CardSubtitle>
    <CardText>
    All types of food is available in our restaurent.
    charminar,kukatpally,Gachibowli..
    </CardText>
    <Button style={{width:250,backgroundColor:'secondary'}}>
    search for items
    </Button>
  </CardBody>
</Card>
  </div> 

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem',
    height: '40'
  }}
>
  <img
    alt="Sample"
    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQD9-R9EFyg0qEpQ0mkuwm_U19YICOTK-DPk_TbkuiESYlPXdQNtymWHds_DCTCQFRdxg&usqp=CAU"/>
  <CardBody>
    <CardTitle tag="h5">
    MEHFILL RESTAURENT
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6">
   </CardSubtitle>
    <CardText>
    All types of food is available in our restaurent.
    charminar,kukatpally,Gachibowli,SR nagar
   </CardText>
    <Button style={{width:250,backgroundColor:'secondary'}}>
      search for items
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem',
    height: '40'
  }}
>
  <img
    alt="Sample"
    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPEfOkJ6qXpRolwrYYi_jXZRllliWaryX5Cg&usqp=CAU"/>
  <CardBody>
    <CardTitle tag="h5">
   TAJ RESTAURENT
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6">
   </CardSubtitle>
    <CardText>
    All types of food is available in our restaurent.
    charminar,kukatpally,Gachibowli,SR nagar
    </CardText>
    <Button style={{width:250,backgroundColor:'secondary'}}>
     search for items
    </Button>
  </CardBody>
</Card>
</div>
  </div> 

  <footer class="w-100 py-4 flex-shrink-0" style={{backgroundColor:'black'}}>
        <div class="container py-4">
            <div class="row gy-4 gx-5">
                <div class="col-lg-4 col-md-6">
                    <h3 class="h1 text-white">Restaurent</h3>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Zomoto Blog</a></li>
                        <li><a href="#">Bug bounty</a></li>
                        <li><a href="#">Zomoto corporate</a></li>
                        <li><a href="#">zomoto instamart</a></li>
                        <li><a href="#">zomoto genie</a></li>
                        <li><a href="#">Zomoto HDFC Bank credit card</a></li>
                        <li><a href="#">ESG</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Quick links</h5>
                    <ul class="list-unstyled text-muted">
                    <li><a href="#">Help & Support</a></li>
                        <li><a href="#">partner with us</a></li>
                        <li><a href="#">Ride with us</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">CONTACT </h5>
                    <ul class="list-unstyled text-muted">
                    <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Get started</a></li>
                        <li><a href="#">FAQ</a></li>
                        
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h5 class="text-white mb-3">Newsletter</h5>
                    <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    <form action="#">
                        <div class="input-group mb-3">
                            <input class="form-control" type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2"></input>
                            <button class="btn btn-primary" id="button-addon2" type="button"><i class="fas fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>


 </div> 
 )
}
 
export default Assignment;