const {createStore} = require ('redux');
const initialstate ={
    bal:1000,
    name:"spandanakethiri",
    accountnumber:687764,
};
const reducer = (state = initialstate, action) =>{
    const newstate = {...state};
    switch(action. type) {
        case "deposit":
            newstate.bal += action.value;
            break;
            case "withdraw":
                newstate.bal -= action.value;
                break;  
    }
    return newstate;
};
const store =createStore(reducer)

store.subscribe(()=>{
    console.log(store.getState())
})

store.dispatch({type:'deposit',value:2000})
store.dispatch({type:'deposit',value:10000})
store.dispatch({type:'withdraw',value:2000})
